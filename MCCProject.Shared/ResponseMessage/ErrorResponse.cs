﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MccProject.Shared.ResponseMessage
{
    public class ErrorResponse : IResponseMessage
    {
        public bool IsSuccess { get; set; }
        public int ResponseCode { get; set; }
        public List<string> ErrorList { get; set; }

        public ErrorResponse(List<string> ErrorList)
        {
            IsSuccess = false;
            ResponseCode = 500;
            this.ErrorList = ErrorList;
        }
    }
}
