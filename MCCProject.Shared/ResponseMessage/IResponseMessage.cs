﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MccProject.Shared.ResponseMessage
{
    public interface IResponseMessage
    {
        bool IsSuccess { get; set; }
        int  ResponseCode { get; set; }
    }
}
