﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MccProject.Shared.ResponseMessage
{
    public class SuccessResponseMessage : IResponseMessage
    {
        public bool IsSuccess { get; set; }
        public int ResponseCode { get; set; }
        public object Response { get; set; }

        public SuccessResponseMessage(object Response)
        {
            IsSuccess = true;
            Response = 200;
            this.Response = Response;
        }
    }
}
