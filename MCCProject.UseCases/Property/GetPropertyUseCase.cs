using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using MediatR;
using MccProject.Infrastructure.DapperWrapper;
using MccProject.Shared.ResponseMessage;
using Dto = MCCProject.Shared.Dtos;

namespace MCCProject.UseCases.Property
{

    public class GetPropertyListUseCase
    {
        public class Model
        {
            public int PropertyId { get; set; }
        }

        public class Query : IRequest<IResponseMessage>
        {
            public int StateId { get; set; }
            public int MunicipioId { get; set; }
        }

        public class QueryHandler : IRequestHandler<Query, IResponseMessage>
        {
            IDapperWrapper dapperWrapper;

            public QueryHandler(IDapperWrapper dapperWrapper)
            {
                this.dapperWrapper = dapperWrapper;
            }


            public async Task<IResponseMessage> Handle(Query request, CancellationToken cancellationToken)
            {
                try
                {
                    dapperWrapper.OpenConnection();
                    var sql = "GetPropertyList";

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@state", request.StateId, DbType.Int32, ParameterDirection.Input);
                    parameters.Add("@municipality", request.MunicipioId, DbType.Int32, ParameterDirection.Input);

                    var propertyList = await dapperWrapper.Connection.QueryAsync<Model>(sql, parameters, commandType: CommandType.StoredProcedure);

                    dapperWrapper.CloseConnection();

                    return new SuccessResponseMessage(propertyList.ToList());
                }
                catch (System.Exception)
                {
                    dapperWrapper.CloseConnection();
                    var errors = new List<string>();
                    errors.Add("Internal Server Error");
                    return new ErrorResponse(errors);
                }
            }
        }
    }
}