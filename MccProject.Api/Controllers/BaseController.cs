﻿using MccProject.Shared.ResponseMessage;
using Microsoft.AspNetCore.Mvc;

namespace MccProject.Api.Controllers
{
    public class BaseController : Controller
    {
        private ObjectResult statusResponse;

        public ObjectResult ResponseMessage(IResponseMessage responseMessage)
        {
            if (responseMessage.IsSuccess)
            {
                var response = responseMessage as SuccessResponseMessage;
                statusResponse = StatusCode(response.ResponseCode, response.Response);
            }
            else
            {
                var response = responseMessage as ErrorResponse;
                statusResponse = StatusCode(response.ResponseCode, response.ErrorList);
            }

            return statusResponse;
        }


    }
}
