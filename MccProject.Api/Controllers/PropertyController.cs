﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using MCCProject.UseCases.Property;

namespace MccProject.Api.Controllers
{
    [Route("api/[controller]")]
    public class PropertyController : BaseController
    {
        IMediator mediator;

        public PropertyController(IMediator mediator) => this.mediator = mediator;

        public async Task<IActionResult> GetList()
        {
            var query = new GetPropertyListUseCase.Query() { MunicipioId = 1, StateId = 1 };
            var response = await mediator.Send(query);

            return ResponseMessage(response);
        }
    }
}
