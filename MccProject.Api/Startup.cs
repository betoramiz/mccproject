﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using MccProject.Core.Repositories;
using MccProject.Infrastructure;
using MccProject.Infrastructure.Repositories;
using UseCases =  MCCProject.UseCases;
using MccProject.Infrastructure.DapperWrapper;
using MediatR;

namespace MccProject.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Infrastructure.Context.IdentityContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("MccConnectionString"))
            );

            services.AddSingleton(new ConnectionString(Configuration.GetConnectionString("MccConnectionString")));

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IAmenityRepository, AmenityRepository>();
            services.AddScoped<IPropertyRepository, PropertyRepository>();
            services.AddScoped<IDapperWrapper, DapperWrapper>();

            services.AddMediatR(typeof(Startup));
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
