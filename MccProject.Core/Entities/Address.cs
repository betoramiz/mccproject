﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MccProject.Core.Entities
{
    public class Address
    {
        public int Id { get; set; }
        public string Steet { get; set; }
        public string Number { get; set; }
        public int State { get; set; }
        public int City { get; set; }        
    }
}
