﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MccProject.Core.Entities
{
    public class Amenity
    {
        public int Id { get; set; }
        public int Rooms { get; set; }
        public int Bathrooms { get; set; }
        public int Floors { get; set; }

        public int PropertyId { get; set; }
        public List<Property> Properties { get; set; }
    }
}
