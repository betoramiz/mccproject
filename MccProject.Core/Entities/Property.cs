﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MccProject.Core.Entities
{
    public class Property
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public decimal Price { get; set; }
        public string PhotosUrl { get; set; }
        public int OperationType { get; set; }

        public Amenity Amenity { get; set; }
        public Address Address { get; set; }
    }
}
