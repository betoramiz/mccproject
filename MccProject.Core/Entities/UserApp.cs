﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace MccProject.Core.Entities
{
    public class UserApp : IdentityUser
    {
        public string Name { get; set; }
        public string LastName { get; set; }

        public int propertyId { get; set; }
        public List<Property> Properties { get; set; }
    }
}
