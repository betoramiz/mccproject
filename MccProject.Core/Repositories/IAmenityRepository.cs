﻿using MccProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MccProject.Core.Repositories
{
    public interface IAmenityRepository : IRepository<Amenity>
    {
    }
}
