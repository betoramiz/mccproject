﻿using MccProject.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MccProject.Infrastructure.Context
{
    public class IdentityContext : DbContext
    {
        public IdentityContext(DbContextOptions<IdentityContext> options) : base(options) { }

        public DbSet<Address> Address { get; set; }
        public DbSet<Amenity> Amenity { get; set; }
        public DbSet<Property> Property { get; set; }
        //public DbSet<UserApp> UserApp { get; set; }
    }
}
