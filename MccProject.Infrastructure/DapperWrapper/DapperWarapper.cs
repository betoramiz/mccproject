using System.Data;
using System.Data.SqlClient;

namespace MccProject.Infrastructure.DapperWrapper
{    
    public class DapperWrapper : IDapperWrapper
    {
        private string sqlConnectionString = string.Empty;

        private SqlConnection _sqlConnection;

        public SqlConnection Connection { get; set; }

        public DapperWrapper(ConnectionString connectionString)
        {
            sqlConnectionString = connectionString.Value;
            var connection = new SqlConnection(connectionString.Value);
            _sqlConnection = connection;
        }
        
        public void OpenConnection()
        {
            if(_sqlConnection.State == ConnectionState.Open)
                Connection = _sqlConnection;

            _sqlConnection.Open();
            Connection = _sqlConnection;
        }

        public void CloseConnection()
        {
            if(_sqlConnection.State == ConnectionState.Open)
                _sqlConnection.Close();
        }
    }
}