using System.Data.SqlClient;

namespace MccProject.Infrastructure.DapperWrapper
{
    public interface IDapperWrapper
    {
        SqlConnection Connection { get; set; }
        void OpenConnection();
        void CloseConnection();
    }
}