﻿using MccProject.Core.Entities;
using MccProject.Core.Repositories;
using MccProject.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace MccProject.Infrastructure.Repositories
{
    public class AmenityRepository : Repository<Amenity>, IAmenityRepository
    {
        public AmenityRepository(IdentityContext context) : base(context) { }
    }
}
