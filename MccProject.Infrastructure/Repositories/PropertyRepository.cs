﻿using MccProject.Core.Entities;
using MccProject.Core.Repositories;
using MccProject.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace MccProject.Infrastructure.Repositories
{
    public class PropertyRepository : Repository<Property>, IPropertyRepository
    {
        public PropertyRepository(IdentityContext context) : base(context) { }
    }
}
