﻿using MccProject.Core.Repositories;
using MccProject.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MccProject.Infrastructure.Repositories
{
    public class Repository<T> : IRepository<T>, IRepositoryAsync<T> where T : class
    {
        private IdentityContext context;
        private DbSet<T> dbSet;

        public Repository(IdentityContext context)
        {
            this.context = context;
            dbSet = context.Set<T>();
        }

        #region Async
        public async Task AddAsync(T entity)
        {
            await dbSet.AddAsync(entity);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            dbSet.Remove(entity);
            await context.SaveChangesAsync();
        }

        public async Task<T> GetByIdAsync(string id)
        {
            var items = await dbSet.FindAsync(id);
            return items;
        }

        public async Task<T> GetByIdAsync(int id)
        {
            var items = await dbSet.FindAsync(id);
            return items;
        }

        public async Task<List<T>> GetByAsync(Expression<Func<T, bool>> predicate)
        {
            return await dbSet.Where(predicate).ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            dbSet.Update(entity);
            await context.SaveChangesAsync();
        }
        #endregion

        #region Syncronus
        public void Add(T entity)
        {
            dbSet.Add(entity);
            context.SaveChanges();
        }        

        public void Delete(T entity)
        {
            dbSet.Remove(entity);
            context.SaveChanges();
        }

        public async Task<List<T>> GetAllAsync()
        {
            var items = await dbSet.ToListAsync();
            return items;
        }

        public T GetById(string id)
        {
            var item = dbSet.Find(id);
            return item;
        }

        public List<T> GetBy(Expression<Func<T, bool>> predicate)
        {
            var items = dbSet.Where(predicate).ToList();
            return items;
        }        

        public T GetById(int id)
        {
            var item = dbSet.Find(id);
            return item;
        }        

        public void Update(T entity)
        {
            dbSet.Update(entity);
            context.SaveChanges();
        }        
        #endregion


    }
}
