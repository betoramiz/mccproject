using System;
using Entities = MccProject.Core.Entities;
using MccProject.Core.Repositories;
using Shouldly;
using Moq;
using Xunit;

namespace MccProject.Test
{
    public class PropertyTest
    {
        private Mock<IPropertyRepository> propertyRepository;
        private Entities.Property initialProperty = null;

        public PropertyTest()
        {
            propertyRepository = new Mock<IPropertyRepository>();
            initialProperty = new Entities.Property (){ Id = 1, Type = 1, Price = 1, Address = new Entities.Address() { }, Amenity = new Entities.Amenity() { }, OperationType = 1, PhotosUrl = "" };
        }

        [Fact]
        public void Should_Add_New()
        {
            //Arrange
            propertyRepository.Setup(x => x.Add(It.IsAny<Entities.Property>())).Verifiable();
            propertyRepository.Setup(x => x.GetById(initialProperty.Id)).Returns(() => initialProperty);

            //Act
            propertyRepository.Object.Add(initialProperty);
            var entityAdded = propertyRepository.Object.GetById(initialProperty.Id);

            //Assert
            propertyRepository.Verify(x => x.Add(It.IsAny<Entities.Property>()), Times.Once());
            entityAdded.Id.ShouldBe(initialProperty.Id);
        }
        [Fact]
        public void Should_Exist()
        {
            var property = new Entities.Property (){ Id = 1, Type = 1, Price = 1, Address = new Entities.Address() { }, Amenity = new Entities.Amenity() { }, OperationType = 1, PhotosUrl = "" };

            //Given
            propertyRepository.Setup(x => x.GetById(It.IsAny<int>())).Returns( property);

            //When
            var entity = propertyRepository.Object.GetById(property.Id);

            //Then
            entity.ShouldNotBe(null);
        }

        [Fact]
        public void should_Remove_One()
        {
            //Given            
            propertyRepository.Setup(x => x.Delete(It.IsAny<Entities.Property>()));

            //When
            propertyRepository.Object.Delete(initialProperty);
            var result = propertyRepository.Object.GetById(1);

            //Then
            propertyRepository.Verify(x => x.Delete(It.IsAny<Entities.Property>()), Times.Once());
            result.ShouldBe(null);
        }

        [Fact]
        public void Should_Update()
        {
            var propertyId = 1;            

            //Given
            propertyRepository.Setup(x => x.Update(It.IsAny<Entities.Property>())).Verifiable();
            propertyRepository.Setup(x => x.GetById(It.IsAny<int>())).Returns(initialProperty);
            

            //When
            var property = propertyRepository.Object.GetById(propertyId);
            property.Price = 2;
            propertyRepository.Object.Update(property);
            var propertyUpdated = propertyRepository.Object.GetById(propertyId);

            //Then
            propertyRepository.Verify(x => x.Update(It.IsAny<Entities.Property>()), Times.Once());
            property.Price.ShouldBe(propertyUpdated.Price);
        }

    }
}
